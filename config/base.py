from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from elasticsearch_dsl import connections
import os

app = FastAPI()

NER_model = "ner_apr_4"

bucket_name = "graduationthesisleo"

craft_detector_output_dir = 'outputs/'

bouding_box = {
    "color": (0, 0, 139),
    "thickness": 10
}

TESSERACT_CONFIG = {
    "eng": r'-l eng --psm 6',
    "vie": r'-l vie --psm 6'
}

origins = [
    "*",
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

elasticsearch_index_name = "document"

hosts = os.environ.get('hosts', 'https://elastic:N5CYIqNAZO1dXpt5FvrpSSmq@gt-es.es.ap-southeast-1.aws.found.io:9243')
print(f"--------------- Connect to ES hosts {hosts}")
# ELASTICSEARCH_DSL = {
#     'default': {
#         # 'dns_name': ['leoesnamespace'],
#         'hosts': hosts,
#         # 'hosts': 'localhost',
#         'port': '9200'
#     },
# }
# connections.configure(**ELASTICSEARCH_DSL)

connections.create_connection(hosts=[hosts], timeout=20)

