from threading import Thread

import cv2
import numpy as np
from PIL import Image
from craft_text_detector import Craft
from vietocr.tool.predictor import Predictor

from config.base import bouding_box
from service import config
from service import get_prediction, measure_duration_processing_sync


class MyCraft(Craft):

    def detect_text(self, image: np.ndarray):
        # perform prediction
        prediction_result = get_prediction(
            image=image,
            craft_net=self.craft_net,
            refine_net=self.refine_net,
            text_threshold=self.text_threshold,
            link_threshold=self.link_threshold,
            low_text=self.low_text,
            cuda=self.cuda,
            long_size=self.long_size,
        )

        # arrange regions
        if self.crop_type == "box":
            regions = prediction_result["boxes"]
        elif self.crop_type == "poly":
            regions = prediction_result["polys"]
        else:
            raise TypeError("crop_type can be only 'polys' or 'boxes'")

        # export if output_dir is given
        prediction_result["text_crop_paths"] = []

        # return prediction results
        return prediction_result


class OCRPerformer1:
    def __init__(self, show_img: bool = False):
        self._show_img: bool = show_img

        # CRAFT
        self._craft = MyCraft(crop_type="poly", cuda=False)

        # VietOCR
        self._detector = Predictor(config)

    def show_img(self, title: str, img: np.ndarray):
        def inner():
            cv2.namedWindow(title, cv2.WINDOW_NORMAL)
            cv2.imshow(title, img)  # Show image
            cv2.waitKey(0)
            cv2.destroyAllWindows()

        if self._show_img:
            Thread(target=inner).start()

    @measure_duration_processing_sync
    def detect_text(self, img: np.ndarray) -> np.ndarray:  # shape (n_bbox, 4, 2)
        return self._craft.detect_text(img)['boxes']

    @measure_duration_processing_sync
    def recognize_text(self, boxes: np.ndarray, img: np.ndarray):
        extracted_text = ""
        for box in boxes:
            x_start = int(box[0, 1])
            x_end = int(box[2, 1])
            y_start = int(box[0, 0])
            y_end = int(box[1, 0])

            cropped = img[x_start:x_end, y_start:y_end]
            if not 0 in cropped.shape:
                # Extract text from Bouding box
                self.show_img("crop box {}".format(box), cropped)
                pil_cropped = Image.fromarray(cropped, 'RGB')
                extracted_text += "\n" + self._detector.predict(pil_cropped, return_prob=False)

                # Draw bounding box in img
                point_top_left = (y_start, x_start)
                point_bot_right = (y_end, x_end)
                img = cv2.rectangle(img, point_top_left, point_bot_right, bouding_box['color'], bouding_box['thickness'])

        self.show_img("drawn img", img)
        return extracted_text, cv2.imencode('.jpg', img)[1].tostring()
