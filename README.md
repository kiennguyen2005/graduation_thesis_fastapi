# graduation_thesis_fastapi
If you run this repository in Docker, you can ignore all of theses guidelines

1. Virtual env for this project. You should create it outside the project\
   cd ..\
python3 -m venv ocr_env\
source ocr_env/bin/activate\
cd graduation_thesis_fastapi
2. Upgrade pip\
python3 -m pip install --upgrade pip
   
3. Install libs\
pip install -r requirements.txt
   
4. Install additional lib
# For Spacy: Vietnamese language model
pip install https://github.com/trungtv/vi_spacy/raw/master/packages/vi_spacy_model-0.2.1/dist/vi_spacy_model-0.2.1.tar.gz
# For Detectron2
pip install git+https://github.com/facebookresearch/fvcore.git
pip install -e detectron2_repo

3. Install Tess\
sudo apt-get install tesseract-ocr-vie
   
4. Runserver\
uvicorn main:app --reload --port 13000